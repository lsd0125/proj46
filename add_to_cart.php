<?php
include __DIR__. '/__connect_db.php';

if(! isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
}


if(isset($_GET['sid'])) {
    $sid = intval($_GET['sid']);

    // 加入購物車, 或者變更數量
    if(isset($_GET['qty'])) {
        $qty = intval($_GET['qty']);
        $_SESSION['cart'][$sid] = $qty;
        /*
        $_SESSION['cart'][$sid] = array(
            'qty' => $qty,
        );
        */
    } else {
        // 刪除項目
        unset($_SESSION['cart'][$sid]);
    }
}

echo json_encode($_SESSION['cart']);



