<?php
include __DIR__. '/__connect_db.php';
$pageName = 'register';

if(isset($_POST['email_id'])){
    // $certi = md5($_POST['email_id']. uniqid()); //md5 32-char
    $certi = sha1($_POST['email_id']. uniqid()); // sha1 40-char

    $statement = $mysqli->prepare("INSERT INTO `members`(
        `sid`, `email_id`, `password`, `nickname`,
        `mobile`, `address`, `created_at`, `modified_at`,
         `certification`, `activated`
         ) VALUES (
         NULL, ?, ?, ?,
          ?, ?, NOW(), NOW(),
          ?, 0)");

    $statement->bind_param("ssssss",
        $_POST['email_id'],
        sha1($_POST['password']),
        $_POST['nickname'],
        $_POST['mobile'],
        $_POST['address'],
        $certi
    );
    $statement->execute();

    $statement->close();

    $msg = "感謝您的註冊";


//    header('Location: show_data.php');
//
//    exit;


}

?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <?php if(isset($msg)): ?>
    <div class="col-lg-12">
        <div class="alert alert-success" role="alert"><?= $msg ?></div>
    </div>
    <?php endif ?>
    <div class="col-lg-6">
        <div class="panel panel-default" style="margin-top: 50px">
            <div class="panel-heading">
                <h3 class="panel-title">會員註冊</h3>
            </div>
            <div class="panel-body">
                <form name="form1" class="form-horizontal" method="post" onsubmit="return formCheck()">
                    <div class="form-group">
                        <label for="email_id" class="col-sm-2 control-label">* 帳號</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email_id" name="email_id" placeholder="電子郵件帳號"
                            value="<?= isset($_POST['email_id']) ? $_POST['email_id'] : '' ?>">
                            <span class="label label-danger" style="display: none;">Danger</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">* 密碼</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password" placeholder="密碼">
                            <span class="label label-danger" style="display: none;">Danger</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password2" class="col-sm-2 control-label">* 密碼確認</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password2" name="password2" placeholder="密碼確認">
                            <span class="label label-danger" style="display: none;">Danger</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nickname" class="col-sm-2 control-label">* 匿稱</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nickname" name="nickname" placeholder="匿稱"
                                   value="<?= isset($_POST['nickname']) ? $_POST['nickname'] : '' ?>">
                            <span class="label label-danger" style="display: none;">Danger</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobile" class="col-sm-2 control-label">手機號碼</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="手機號碼"
                                   value="<?= isset($_POST['mobile']) ? $_POST['mobile'] : '' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">地址</label>
                        <div class="col-sm-10">
                            <textarea  class="form-control" name="address" id="address" cols="30" rows="10"><?= isset($_POST['address']) ? $_POST['address'] : '' ?></textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">加入</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<?php include __DIR__. '/__html_foot.php'; ?>
<script>
    function formCheck() {
        var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var email_id = $('#email_id').val();
        var password = $('#password').val();
        var password2 = $('#password2').val();
        var nickname = $('#nickname').val();
        var isPass = true;
        var span;

        $('form span.label-danger').hide();
        span = $('#email_id').next();
        if(! email_id){
            span.show().text('請填寫 email');
            isPass = false;
        }
        if(! pattern.test(email_id)){
            span.show().text('請填寫正確的 email 格式');
            isPass = false;
        }
        if(password.length <6){
            span = $('#password').next();
            span.show().text('請填寫密碼');
            isPass = false;
        }
        if( password != password2){
            span = $('#password2').next();
            span.show().text('密碼和密碼確認的值不同');
            isPass = false;
        }
        if(! nickname){
            span = $('#nickname').next();
            span.show().text('請填寫匿稱');
            isPass = false;
        }

        return isPass;
    }
    <?php if(isset($msg)): ?>
        $(form1.elements).prop('disabled', 'disabled');
    <?php endif ?>
    /*
    // 作法一
    function formCheck() {
        var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var email_id = $('#email_id').val();
        var password = $('#password').val();
        var password2 = $('#password2').val();
        var nickname = $('#nickname').val();
        if(! email_id){
            alert('請填寫 email');
            return false;
        }
        if(! pattern.test(email_id)){
            alert('請填寫正確的 email 格式');
            return false;
        }
        if(password.length <6){
            alert('請填寫長度為 6 以上的密碼');
            return false;
        }
        if( password != password2){
            alert('密碼和密碼確認的值不同');
            return false;
        }
        if(! nickname){
            alert('請填寫匿稱');
            return false;
        }

        return true;
    }
    */

</script>
