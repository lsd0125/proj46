    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="collapsed navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                <a href="./" class="navbar-brand">LOGO</a></div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?= (isset($pageName) and $pageName=='product_list') ? 'active' : '' ?>"><a href="product_list.php">商品列表</a></li>
                    <li class="<?= (isset($pageName) and $pageName=='cart') ? 'active' : '' ?>">
                        <a href="cart.php">購物車<span class="badge cart-qty">0</span></a>
                    </li>
                    <li class="<?= (isset($pageName) and $pageName=='product_list2') ? 'active' : '' ?>"><a href="product_list2.php">商品列表2</a></li>
                    <li class="<?= (isset($pageName) and $pageName=='product_list3') ? 'active' : '' ?>"><a href="product_list3.php">商品列表3</a></li>


                </ul>
                <?php if(isset($_SESSION['user'])):  ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><?= $_SESSION['user']['nickname'] ?></a></li>
                        <li><a href="logout.php">登出</a></li>
                    </ul>
                <?php else:  ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="<?= (isset($pageName) and $pageName=='login') ? 'active' : '' ?>"><a href="login2.php">登入</a></li>
                        <li class="<?= (isset($pageName) and $pageName=='register') ? 'active' : '' ?>"><a href="register.php">註冊</a></li>
                    </ul>
                <?php endif  ?>

            </div>
        </div>
    </nav>
