<?php
include __DIR__. '/__connect_db.php';
$pageName = 'login';

?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <div class="col-lg-6">
        <div class="panel panel-default" style="margin-top: 50px">
            <div class="panel-heading">
                <h3 class="panel-title">會員登入</h3>
            </div>
            <div class="panel-body">
                <form id="form1" class="form-horizontal" method="post" onsubmit="return checkForm()">
                    <div class="form-group">
                        <label for="email_id" class="col-sm-2 control-label">* 帳號</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email_id" name="email_id" placeholder="電子郵件帳號"
                                   value="<?= isset($_POST['email_id']) ? $_POST['email_id'] : '' ?>">
<!--                            <span class="label label-danger" style="display: none;">Danger</span>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">* 密碼</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password" placeholder="密碼">
<!--                            <span class="label label-danger" style="display: none;">Danger</span>-->
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">登入</button>
                        </div>
                    </div>
                </form>
                <a id="mytest" href="#test_cbox" style="display:none">test</a>
                <div style="display:none">
                    <div id="test_cbox" >
                        <div>
                            dfgsf<br>
                            dfgsf<br>
                            dfgsf<br>
                            dfgsf<br>
                            dfgsf<br>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php include __DIR__. '/__html_foot.php'; ?>
<script>
    function checkForm() {
        // 檢查欄位輸入值的長度或格式

        $.post('login2_ajax.php', $('#form1').serialize(), function(data){
            //console.log(data);
            // alert(data.msg);
            $('#test_cbox').html(data.msg);
            $('#mytest').click();
//            if(data.success){
//                location.href = './';
//            }
        }, 'json' );
        return false;
    }

    $('#mytest').colorbox({inline:true, width:"50%", height:"200px"});
</script>
