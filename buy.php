<?php
include __DIR__. '/__connect_db.php';
$pageName = 'buy';

if(! isset($_SESSION['user'])){
    header("Location: cart.php");
    exit;
}


$cart_data = array();
if(isset($_SESSION['cart']) and !empty($_SESSION['cart'])) {
    $sids = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $sids));

    $result = $mysqli->query($sql);

    while($row=$result->fetch_assoc()){
        $cart_data[$row['sid']] = $row;
    }
} else {
    $no_data = true;
}


?>
<?php include __DIR__. '/__html_head.php'; ?>
    <div class="container">
        <?php include __DIR__. '/__navbar.php'; ?>
        <style>
            .glyphicon-remove-sign {
                color: orangered;
                font-size: 20px;
                cursor: pointer;
            }
        </style>
    <?php if(isset($no_data)): ?>
        <div class="alert alert-danger" role="alert">購物車內無商品</div>
    <?php else: ?>
    <div class="alert alert-success" role="alert">感謝購買</div>
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>封面</th>
                <th>書名</th>
                <th>單價</th>
                <th>數量</th>
                <th>小計</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $total = 0;
            foreach($_SESSION['cart'] as $sid=>$qty):
                $row = $cart_data[$sid];
                $total += $row['price']*$qty;
                ?>
            <tr class="item-tr" data-sid="<?= $sid ?>" data-price="<?= $row['price'] ?>">

                <td><img src="imgs/small/<?= $row['book_id'] ?>.jpg"></td>
                <td><?= $row['bookname'] ?></td>
                <td><?= $row['price'] ?></td>
                <td><?= $qty ?></td>
                <td class="sub-total"><?= $row['price']*$qty ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="col-lg-12" style="font-size: 24px">
            總計: <span class="label label-info total-price"><?= $total ?></span>
        </div>

<!--
<pre>
    <?php print_r($cart_data) ?>
</pre>
-->
    </div>
    <?php
        $sql = sprintf("INSERT INTO `orders`
            (`member_sid`, `amount`, `order_date`) VALUES(%s, %s, NOW()) ",
            $_SESSION['user']['sid'],
            $total
            );
        $mysqli->query($sql);

        $order_sid = $mysqli->insert_id;

        foreach($_SESSION['cart'] as $sid=>$qty){
            $sql = sprintf("INSERT INTO `order_details`
            (`order_sid`, `product_sid`, `price`, `quantity`) VALUES (%s, %s, %s, %s)",
            $order_sid,
            $sid,
            $cart_data[$sid]['price'],
            intval($qty)
            );
            $mysqli->query($sql);
        }

        unset($_SESSION['cart']);

    endif;

    ?>
    </div>
<?php include __DIR__. '/__html_foot.php'; ?>
<script>

</script>

