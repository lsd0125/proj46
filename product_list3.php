<?php
include __DIR__. '/__connect_db.php';
$pageName = 'product_list3';


// 分類資料
$c_result = $mysqli->query("SELECT * FROM `categories` WHERE parent_sid=0 ORDER BY sid ASC");



$sql = sprintf("SELECT * FROM `products`");

$result = $mysqli->query($sql);

$p_data = array();

while($row = $result->fetch_assoc()){
    $p_data[$row['sid']] = $row;
}
?>
<?php include __DIR__. '/__html_head.php'; ?>
    <div class="container">
        <?php include __DIR__. '/__navbar.php'; ?>

    <div class="col-lg-12">
        <div class="col-lg-3">

            <div class="btn-group-vertical col-lg-12" role="group" aria-label="Vertical button group">
                <a href="#0" class="btn btn-default">
                    全部商品
                </a>
                <?php while($row = $c_result->fetch_assoc()): ?>
                <a href="#<?= $row['sid'] ?>" class="btn btn-default">
                    <?= $row['name'] ?>
                </a>
                <?php endwhile; ?>
            </div>

        </div>

        <div class="col-lg-9" id="product_list">


        </div>

    </div>

    </div>
<?php include __DIR__. '/__html_foot.php'; ?>
<script type="text/x-template" id="tplTxt">
    <div class="col-lg-3 col-sm-4 col-xs-6 sd_product" style="display:none" data-cate="<%= category_sid %>">
        <div class="thumbnail" style="height:280px; margin:10px 0;">
            <a class="single_product" href="" target="_blank">
                <img src="imgs/small/<%= book_id %>.jpg" style="width: 100px; height: 135px;">
            </a>
            <div class="caption">
                <h5><%= bookname %></h5>
                <h5><%= author %></h5>
                <p>
                    <span class="glyphicon glyphicon-search"></span>
                    <span class="label label-info">$ <%= price %></span>
                    <select name="qty" class="qty">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                    <button class="btn btn-warning btn-sm buy_btn" data-sid="<%= sid %>">買</button>
                </p>
            </div>
        </div>
    </div>
</script>

<script>
    var p_data = <?php echo json_encode($p_data, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE); ?>;

    var p_tpl_func = _.template( $('#tplTxt').text() );
    var p_list = $('#product_list');

    for(var s in p_data) {
        p_list.append( p_tpl_func(p_data[s]) );
    }


    var showProducts = function(cate_sid){
        cate_sid = cate_sid ? parseInt(cate_sid) : 0;
        var sd_product = $('.sd_product');

        sd_product.each(function(){
            var cate = $(this).attr('data-cate');
            if(cate_sid==0) {
                $(this).fadeIn();
            } else {
                if(cate==cate_sid) {
                    $(this).fadeIn();
                } else {
                    $(this).fadeOut();
                }
            }
        });

    };
    var hashHandler = function(){
        var h = location.hash.slice(1);
        showProducts(h);
    };
    window.addEventListener('hashchange', hashHandler);

    hashHandler();
</script>

