<?php
include __DIR__. '/__connect_db.php';
$pageName = 'product_list';
$per_page = 4; //一頁有幾筆資料

// 客戶端指定第幾頁
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;

// 分類編號
$cate_sid = isset($_GET['cate_sid']) ? intval($_GET['cate_sid']) : 0;

// 搜尋
$search = isset($_GET['search']) ? $_GET['search'] : '';

//條件
$where = " WHERE 1 ";
$qstr = '';
if($cate_sid != 0){
    $where .= " AND category_sid = $cate_sid ";
    $qstr .= "&cate_sid=". $cate_sid;
}

if($search != '') {
    $s = "%{$search}%";
    $s = $mysqli->escape_string( $s );

    $where .= sprintf(" AND (`author` LIKE '%s' OR `bookname` LIKE '%s') ", $s, $s);
    $qstr .= "&search=". $search;
}


// 分類資料
$c_result = $mysqli->query("SELECT * FROM `categories` WHERE parent_sid=0 ORDER BY sid ASC");

// 算總筆數
$t_result = $mysqli->query("SELECT 1 FROM `products` $where");
$num_rows = $t_result->num_rows; // 取得總筆數
$total_pages = ceil($num_rows/$per_page); // 算出總頁數

$sql = sprintf("SELECT * FROM `products` %s LIMIT %s, %s", $where, ($page-1)*$per_page, $per_page);
// echo $sql. "<br>";
$result = $mysqli->query($sql);

$g_ok = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';


$page_start = ($page -2)>0 ? ($page -2) : 1;
$page_end = ($page_start + 4) <= $total_pages ? ($page_start + 4) : $total_pages;
if($page_end-$page_start < 4) {
    $page_start = $page_end-4;
    while($page_start<1){
        $page_start++;
    }
}

?>
<?php include __DIR__. '/__html_head.php'; ?>
    <div class="container">
        <?php include __DIR__. '/__navbar.php'; ?>

    <div class="col-lg-12">
        <div class="col-lg-3">

            <div class="btn-group-vertical col-lg-12" role="group" aria-label="Vertical button group">
                <a href="product_list.php" class="btn btn-default">
                    <?= 0==$cate_sid ? $g_ok : '' ?>全部商品
                </a>
                <?php while($row = $c_result->fetch_assoc()): ?>
                <a href="?cate_sid=<?= $row['sid'] ?>" class="btn btn-default">
                    <?= $row['sid']==$cate_sid ? $g_ok : '' ?>
                    <?= $row['name'] ?>
                </a>
                <?php endwhile; ?>
            </div>

        </div>
        <div class="col-lg-9">
            <div class="col-lg-12">
                <div class="col-lg-6">
                    <ul class="pagination">
                        <?php if($page>1): ?>
                            <li><a href="?page=<?= ($page-1). $qstr ?>" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                        <?php endif; ?>
                        <?php for($i=$page_start; $i<=$page_end; $i++ ): ?>
                            <li class="<?= $page==$i ? 'active' : '' ?>"><a href="?page=<?= $i. $qstr ?>"><?=$i?></a></li>
                        <?php endfor; ?>
                        <?php if($page<$total_pages): ?>
                            <li><a href="?page=<?= ($page+1). $qstr ?>" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <form class="form-inline" method="get">
                        <div class="form-group">
                            <input type="text" class="form-control" name="search" placeholder="Search"
                            value="<?= isset($_GET['search']) ? $_GET['search'] : '' ?>">
                            <input type="hidden" name="cate_sid"
                                   value="<?= isset($_GET['cate_sid']) ? $_GET['cate_sid'] : 0 ?>">
                        </div>
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </form>
                </div>
            </div>
            <?php while($row = $result->fetch_assoc()): ?>
            <div class="col-lg-3 col-sm-4 col-xs-6">
                <div class="thumbnail" style="height:280px; margin:10px 0;">
                    <a class="single_product" href="single_product.php?sid=23" target="_blank">
                        <img src="imgs/small/<?= $row['book_id'] ?>.jpg" style="width: 100px; height: 135px;">
                    </a>
                    <div class="caption">
                        <h5><?= $row['bookname'] ?></h5>
                        <h5><?= $row['author'] ?></h5>
                        <p>
                            <span class="glyphicon glyphicon-search"></span>
                            <span class="label label-info">$ <?= $row['price'] ?></span>
                            <select name="qty" class="qty">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                            <button class="btn btn-warning btn-sm buy_btn" data-sid="<?= $row['sid'] ?>">買</button>
                        </p>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>

        </div>

    </div>

    </div>
<?php include __DIR__. '/__html_foot.php'; ?>
<script>
    $('.buy_btn').click(function(){
        var sid = $(this).attr('data-sid');
        var qty = $(this).prev().val();

        $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
            console.log(data);
            setCartQty();
            alert("商品已加入購物車");
        });


    });

</script>

