var setCartQty = function(jsonObj) {
    var count = 0;
    if(jsonObj) {
        for(var s in jsonObj){
            count += jsonObj[s];
        }
        $('.cart-qty').text(count);
    } else {
        $.get('add_to_cart.php', function(data){
            var count = 0;
            for(var s in data){
                count += data[s];
            }

            $('.cart-qty').text(count);
        }, 'json');
    }
};


$(function(){
    setCartQty();

});