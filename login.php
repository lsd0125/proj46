<?php
include __DIR__. '/__connect_db.php';
$pageName = 'login';

// DB 的 email_id 要設定成唯一鍵 (unique)
if(isset($_POST['email_id']) and isset($_POST['password'])){
    $sql = sprintf("SELECT * FROM `members` WHERE `email_id`='%s' AND `password`='%s'",
        $mysqli->escape_string($_POST['email_id']),
        sha1($_POST['password'])
        );

    $result = $mysqli->query($sql);

    if($result->num_rows != 0){
        $_SESSION['user'] = $result->fetch_assoc();

        header('Location: ./');
        exit;
    } else {

    }
}


?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <div class="col-lg-6">
        <div class="panel panel-default" style="margin-top: 50px">
            <div class="panel-heading">
                <h3 class="panel-title">會員登入</h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post">
                    <div class="form-group">
                        <label for="email_id" class="col-sm-2 control-label">* 帳號</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email_id" name="email_id" placeholder="電子郵件帳號"
                                   value="<?= isset($_POST['email_id']) ? $_POST['email_id'] : '' ?>">
<!--                            <span class="label label-danger" style="display: none;">Danger</span>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">* 密碼</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password" placeholder="密碼">
<!--                            <span class="label label-danger" style="display: none;">Danger</span>-->
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">登入</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<?php include __DIR__. '/__html_foot.php'; ?>