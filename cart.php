<?php
include __DIR__. '/__connect_db.php';
$pageName = 'cart';
$cart_data = array();
if(isset($_SESSION['cart']) and !empty($_SESSION['cart'])) {
    $sids = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $sids));

    $result = $mysqli->query($sql);

    while($row=$result->fetch_assoc()){
        $cart_data[$row['sid']] = $row;
    }
} else {
    $no_data = true;
}


?>
<?php include __DIR__. '/__html_head.php'; ?>
    <div class="container">
        <?php include __DIR__. '/__navbar.php'; ?>
        <style>
            .glyphicon-remove-sign {
                color: orangered;
                font-size: 20px;
                cursor: pointer;
            }
        </style>
    <?php if(isset($no_data)): ?>
        <div class="alert alert-danger" role="alert">購物車內無商品</div>
    <?php else: ?>
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>取消</th>
                <th>封面</th>
                <th>書名</th>
                <th>單價</th>
                <th>數量</th>
                <th>小計</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($_SESSION['cart'] as $sid=>$qty):
                $row = $cart_data[$sid];
                ?>
            <tr class="item-tr" data-sid="<?= $sid ?>" data-price="<?= $row['price'] ?>">
                <td>
                    <span class="glyphicon glyphicon-remove-sign remove-item" aria-hidden="true"></span>
                </td>
                <td><img src="imgs/small/<?= $row['book_id'] ?>.jpg"></td>
                <td><?= $row['bookname'] ?></td>
                <td><?= $row['price'] ?></td>
                <td>
                    <select class="modify-sel" data-val="<?= $qty ?>">
                        <?php for($i=1; $i<=20; $i++): ?>
                            <option value="<?= $i ?>" <?= $i==$qty ? 'selected' : '' ?>><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                    </td>
                <td class="sub-total"><?= $row['price']*$qty ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="col-lg-12" style="font-size: 24px">
            總計: <span class="label label-info total-price">123</span>
        </div>
        <?php if(isset($_SESSION['user'])): ?>
            <a href="buy.php" type="button" class="btn btn-primary">結帳</a>
        <?php else: ?>
            <div class="col-lg-12" style="padding-top: 20px">
                <div class="alert alert-danger" role="alert">請先登入會員再結帳</div>
            </div>
        <?php endif; ?>

<!--
<pre>
    <?php print_r($cart_data) ?>
</pre>
-->
    </div>
    <?php endif; ?>
    </div>
<?php include __DIR__. '/__html_foot.php'; ?>
<script>
    $('.remove-item').click(function(){
        var tr = $(this).closest('tr');
        var sid = tr.attr('data-sid');
        $.get('add_to_cart.php', {sid:sid}, function(data){
            tr.remove();
            calTotal();
            setCartQty(data);
        }, 'json');
    });

    $('.modify-sel').change(function(){
        var tr = $(this).closest('tr');
        var sid = tr.attr('data-sid');
        var qty = $(this).val();
        var price = tr.attr('data-price');
        $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
            tr.find('.sub-total').text( qty*price );
            calTotal();
            setCartQty(data);
        }, 'json');
    });

    var calTotal = function(){
        var total = 0;
        $('.item-tr').each(function(){
            var price = $(this).attr('data-price');
            var qty = $(this).find('.modify-sel').val();
            total += price*qty;
        });

        $('.total-price').text(total);


    };
    calTotal();
</script>

