SELECT * FROM `products` WHERE `sid` IN (10,14,21,6);

SELECT * FROM `products` WHERE `author` LIKE '%科技%' OR `bookname` LIKE '%科技%';

SELECT * FROM `products` WHERE `author` LIKE '%計%' OR `bookname` LIKE '%計%';

SELECT * FROM `products` WHERE `author` LIKE '%陳%';

SELECT c1.*, c2.name parent_name FROM `categories` c1 LEFT JOIN `categories` c2 ON c1.parent_sid=c2.sid;

SELECT p.*, c.`name` `cate_name` FROM `products` p LEFT JOIN `categories` c ON p.`category_sid`=c.`sid`;

SELECT p.*, c.`name` `cate_name` FROM `products` p INNER JOIN `categories` c ON p.`category_sid`=c.`sid`;

SELECT p.*, c.`name` AS `cate_name` FROM `products` AS p JOIN `categories` AS c ON p.`category_sid`=c.`sid`;

SELECT `products`.*, `categories`.`name` FROM `products` JOIN `categories` ON `products`.`category_sid`=`categories`.`sid`;

SELECT * FROM `products` JOIN `categories` ON `products`.`category_sid`=`categories`.`sid`;

SELECT * FROM `products` JOIN `categories`; -- 通常不這樣用
-- -------------------------------------------
CREATE TABLE `members` (
  `sid` int(11) NOT NULL,
  `email_id` varchar(255) NOT NULL COMMENT 'email 當帳號',
  `password` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `certification` varchar(255) NOT NULL,
  `activated` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`sid`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `members`
--
ALTER TABLE `members`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT;